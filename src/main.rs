#[macro_use]
extern crate text_io;

fn main() {
    use std::time::Duration;
    println!("Do you want an explanation of this program?[y/n]");
    let explain: String = read!();
    if explain == "y"{
        println!("This program is based on a math problem called the collatz \
        conjecture, or 3x+1.\nIt is a set of two rules: a number is chosen.\n\
        If the number chosen is odd, then multiply by 3 and add 1.\nIf the \
        number is even, divide by 2.\nIn this way, each number will have a \
        unique pattern that it follows,\nbut all numbers will end up as 1 \
        eventually.\nThen the calculation loops from 1, to 4, to 2, to 1 \
        indefinitely.");
    }
    println!("Which number you wish to process?");
    let input: i64 = read!();
    println!("How much delay do you want between calculations(in seconds)?");
    let delay = Duration::from_secs(read!());
    println!("You selected {}", input);
    let resultvec = calculate(input, delay);
    println!("The number became 1, and the loop was stopped because
    this calculation loops infinitely from 4-2-1");
    println!("See the index of results? [y/n]");
    let input: String = read!();
    if input == "y"{
        println!("All the numbers are:\n{:?}", resultvec);
    }
}  
fn calculate(mut input: i64, delay: std::time::Duration) -> Vec<i64> {
    use std::thread::sleep;
    let mut resultvec: Vec<i64> = Vec::new();
    let mut sauce = 0;
    while input != 1 {
        if input % 2 == 0 {
            println!("Number is even: {}, dividing by 2", input);
            input /= 2;
            resultvec.push(input);
            sauce += 1;
            println!("Result is: {}", input);
            sleep(delay);
        }
        else if input % 2 != 0 {
            println!("Number is odd: {}, multiplying by 3 then adding 1",input);
            input = input * 3 + 1;
            resultvec.push(input);
            sauce += 1;
            println!("Result is: {}", input);
            sleep(delay);
        }
    }    
    println!("{} caclulations have taken place.", sauce);
    resultvec
}
